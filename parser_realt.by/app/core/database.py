import csv
from multiprocessing.dummy import Pool as ThreadPool
from pymongo import MongoClient
from pymongo import helpers
from app.core import parser


def init_db():
    client = MongoClient()
    return client.flats_db


DB = init_db()


def clear_db():
    DB.flats.delete_many({})


def write_csv(data):
    with open("realt_by.csv", "a") as file:
        writer = csv.writer(file)

        writer.writerow((data["region"],
                         data["locality"],
                         data["address"],
                         data["district_city"],
                         data["district_area"],
                         data["cost_square_meter"],
                         data["full_area"],
                         data["url"],
                         data["telephones"],
                         data["agency"],
                         data["count_rooms"],
                         data["floor_floor"],
                         data["build_type"],
                         data["layout"],
                         data["ceiling_height"],
                         data["year_built"],
                         data["bathroom"],
                         data["exposed"],
                         data["notes"],
                         data["title"],
                         data["photo_urls"],
                         data["_id"],
                         data["coordinate_x"],
                         data["coordinate_y"]))


def chunks_urls(urls, chunk_size=5):
    for j in range(0, len(urls), chunk_size):
        yield urls[j:j + chunk_size]


def chunks_flats(collection, chunk_size=5):
    for j in range(0, collection.count(), chunk_size):
        yield collection.find().skip(j * chunk_size).limit(chunk_size)


def insert_data_in_collection(collection, data):
    collection.insert_one(data)
    write_csv(data)


def insert_page_data_in_db(chunk_urls):
    for url in chunk_urls:
        for data in parser.generator_flats_data(url):
            if data:
                try:
                    insert_data_in_collection(DB.flats, data)
                except helpers.DuplicateKeyError:
                    pass


def update_collection_by_cost(collection_flats, url, cost):
    collection_flats.update_one({"url": url},
                                {"$set": {"cost_square_meter": cost}})


def update_db_with_pages(pages_url):
    for page_url in pages_url:
        parser.update_collection_with_page(page_url, DB.flats)


def find_flat_by_url(db, url):
    return db.find_one({"url": url})


def create_new_db(max_pages=None, processes=10):
    with open("realty_by.csv", "w"):
        pass
    clear_db()

    all_pages_url = parser.get_all_pages_url(parser.BASE_URL + str(0), max_pages)
    with ThreadPool(processes) as pool:
        pool.map(insert_page_data_in_db, chunks_urls(all_pages_url))


def remove_sold_flats_from_db(flats):
    urls = parser.get_urls_to_remove(flats)
    for url in urls:
        DB.flats.remove({"url": url})
        print(url, "deleted")


def update_all_db(processes=10):
    all_pages_url = parser.get_all_pages_url(parser.BASE_URL + str(0))
    with ThreadPool(processes) as pool:
        pool.map(remove_sold_flats_from_db, chunks_flats(DB.flats))
        pool.map(update_db_with_pages, chunks_urls(all_pages_url))


def get_flats_data(n, pagesize=30):
    return DB.flats.find().skip(pagesize*(n-1)).limit(pagesize)


def search_data(value):
    return DB.flats.aggregate([
        {"$match": {
            "$or": [
                {'region': {'$regex': value, '$options': 'i'}},
                {'locality': {'$regex': value, '$options': 'i'}}
            ]
        }}])


def get_flat_data(_id):
    return DB.flats.find_one({"_id": _id})


def main():
    update_all_db()


if __name__ == "__main__":
    main()
