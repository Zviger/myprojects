from app.core.database import (
    update_all_db
)
from .celery_config import celery

update_all_db = celery.task(update_all_db)
