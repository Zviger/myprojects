"""
Functions to parse realt.com
"""
from datetime import datetime
import requests
from bs4 import BeautifulSoup
from app.core import database
BASE_URL = "https://realt.by/sale/flats/?page="

SESSION = requests.Session()

FIELD_MAPPING = {"Область": "region",
                 "Населенный пункт": "locality",
                 "Район города": "district_city",
                 "Район (области)": "district_area",
                 "Дата обновления": "update_date",
                 "Агентство": "agency",
                 # TODO кол-во комнат
                 "Комнат всего/разд.": "count_rooms",
                 "Этаж / этажность": "floor_floor",
                 "Тип дома": "build_type",
                 "Планировка": "layout",
                 "Высота потолков": "ceiling_height",
                 "Год постройки": "year_built",
                 "Сан/узел": "bathroom",
                 "Примечания": "notes"}


def apply_field_mapping(field_mapping, source):
    """
    Get data from source by FIELD_MAPPING and mapping with source
    :param field_mapping: Fields to mapping.
    :param source: A dictionary whose key is a Cyrillic field name, and a value is its value.
    :return: Dictionary for saving in data base.
    """
    data = {}
    for key in field_mapping:
        value = source.get(key)
        data[field_mapping[key]] = value
    return data


def get_html(url):
    """
    Gets html by URL.
    :param url: URL of page
    :return: HTML of page
    """
    request = SESSION.get(url)
    return request.text


def get_total_pages(html):
    """
    Gets all pages for parsing.
    :param html: HTML of page with information about total count of pages.
    :return: Total count of pages.
    """
    soap = BeautifulSoup(html, "lxml")
    pages = soap.find("div", class_="uni-paging").text.strip().split(" ")[-1]
    return int(pages)


def get_all_pages_url(start_url, max_pages=None):
    """
    Gets all URLs for parsing by the number of pages.
    :param start_url:
    :param max_pages:
    :return:
    """
    urls = []
    if not max_pages:
        total_pages = get_total_pages(get_html(start_url))
        for i in range(total_pages)[1:]:  # because the first page contains apartments from other pages
            urls.append(BASE_URL + str(i))
    else:
        for i in range(max_pages)[1:]:
            urls.append(BASE_URL + str(i))
    return urls


def get_flat_data(flat_page_url):
    """
    Receives information about the apartment by URL of flat page.
    :param flat_page_url: URL of flat.
    :return: Dictionary for saving in data base.
    """
    print(flat_page_url)

    flat_page_html = get_html(flat_page_url)

    flat_page_soap = BeautifulSoup(flat_page_html, "lxml")

    _id = flat_page_url.split("/")[-2]

    title = flat_page_soap.find("h1", class_="f24").contents
    if title[0] == " ":
        title = title[2].lstrip()
    else:
        title = title[0].lstrip()

    photo_urls = []
    try:
        photos = flat_page_soap.find("div", class_="photos").find_all("div", class_="photo-item")
        for photo in photos:
            p_url = photo.find("a")
            if p_url:
                photo_urls.append(p_url.get("href"))
            else:
                photo_urls.append(photo.find("img").get("src"))
    except AttributeError:
        pass

    try:
        all_table_zebra = flat_page_soap.find_all("table", class_="table-zebra")
    except AttributeError:
        all_table_zebra = None
        print(flat_page_url, "ошибка")

    try:
        map_info = flat_page_soap.find("div", class_="buildings-map").find("div").find("div").get("data-center")
        coordinates = map_info.split("\"")
        coordinate_x = float(coordinates[9])
        coordinate_y = float(coordinates[13])
    except AttributeError:
        coordinate_x = None
        coordinate_y = None

    left_data = []
    right_data = []
    for tabel in all_table_zebra:
        try:
            left_data += [data.find("td", class_="table-row-left").text for
                          data in tabel.find_all("tr", class_="table-row")]
        except AttributeError:
            left_data.append("")

        try:
            right_data += [data.find("td", class_="table-row-right").text for
                           data in tabel.find_all("tr", class_="table-row")]

        except AttributeError:
            right_data.append("")

    some_data = dict(zip(left_data, right_data))

    result_data = apply_field_mapping(FIELD_MAPPING, some_data)

    try:
        areas = some_data["Площадь общая/жилая/кухня"].strip(" м²").split(" / ")  # area
        try:
            full_area = float(areas[0])
        except ValueError:
            full_area = None
    except KeyError:
        print(flat_page_url, "Ошибка площади")
        return None

    today = datetime.now()
    today = datetime(today.year, today.month - 1, today.day, today.hour,
                     today.minute, today.second).strftime("%Y:%m:%d:%H:%M:%S")
    try:
        cost_square_meter = some_data["Ориентировочная стоимость эквивалентна"].split(" ")[4: 5]
        if "руб/кв.м." in cost_square_meter[0]:
            cost_square_meter = [data.replace("\xa0", "").strip("руб/кв.м.") for data in cost_square_meter]
            try:
                cost_square_meter = {today: int(cost_square_meter[0])}
            except ValueError:
                return None
        else:
            cost_square_meter = [data.replace("\xa0", "").strip("руб/кв.м.") for data in cost_square_meter]
            try:
                cost_square_meter = int(cost_square_meter[0])
                cost_square_meter //= full_area
                cost_square_meter = {today: cost_square_meter}
            except (ValueError, TypeError):
                return None
    except KeyError:
        print(flat_page_url, "Ошбика цены")
        return None

    telephones = some_data.get("Телефоны")
    if telephones:
        telephones = telephones.rstrip().split("+")[1:]
    address = some_data.get("Адрес")

    if address:
        address = address.strip("Информация о доме")

    exposed = True

    dop_data = {"address": address,
                "cost_square_meter": cost_square_meter,
                "full_area": full_area,
                "url": flat_page_url,
                "telephones": telephones,
                "exposed": exposed,
                "title": title,
                "photo_urls": photo_urls,
                "_id": _id,
                "coordinate_x": coordinate_x,
                "coordinate_y": coordinate_y}

    for data in dop_data:
        result_data[data] = dop_data[data]
    print(flat_page_url, " добавлен")
    return result_data


def generator_flats_data(page_url):
    """
    Generates information about apartments on the page with previews of apartments.
    :param page_url: URL of page with previews of flats.
    :return: Iterator.
    """
    html = get_html(page_url)
    soap = BeautifulSoup(html, "lxml")
    flats = []
    try:
        flats = soap.find("div", class_="tx-uedb").find_all("div", class_="bd-item")
    except AttributeError:
        yield None
    for flat in flats:
        flat_page_url = flat.find("a").get("href")
        yield get_flat_data(flat_page_url)


def update_collection_with_flat(flat, today, collection_flats):
    """
    It updates the cost of the apartment by the URL of the apartment, if it exists in the database.
    Otherwise, adds an apartment to the database.
    :param flat: Some HTML data of flat.
    :param today: String with datetime.
    :param collection_flats: Collection of flats in data base.
    :return: None
    """
    flat_page_url = flat.find("div", class_="title").find("a").get("href")
    db_flat = database.find_flat_by_url(collection_flats, flat_page_url)
    if db_flat:
        try:
            current_cost_square_meter = flat.find("div", class_="bd-item-left-bottom-right"). \
                find("span", class_="price-byr").text
            if "руб/кв.м." in current_cost_square_meter:
                current_cost_square_meter = list(current_cost_square_meter.strip("руб/кв.м.").
                                                 split(" "))
                current_cost_square_meter = int(current_cost_square_meter[-1].replace("\xa0", ""))
                try:
                    cost_square_meter = db_flat["cost_square_meter"]
                    cost_square_meter[today] = current_cost_square_meter
                    database.update_collection_by_cost(collection_flats, flat_page_url, cost_square_meter)
                except ValueError:
                    collection_flats.update_one({"url": flat_page_url}, {"$set": {"exposed": False}})
            elif "Цена" not in current_cost_square_meter:
                current_cost_square_meter = int(current_cost_square_meter.strip("руб,").
                                                replace("\xa0", ""))
                try:
                    cost_square_meter = db_flat["cost_square_meter"]
                    full_area = db_flat["full_area"]
                    current_cost_square_meter //= full_area
                    cost_square_meter[today] = current_cost_square_meter
                    database.update_collection_by_cost(collection_flats, flat_page_url, cost_square_meter)

                except (ValueError, TypeError):
                    collection_flats.update_one({"url": flat_page_url}, {"$set": {"exposed": False}})
            else:
                cost_square_meter = db_flat["cost_square_meter"]
                cost_square_meter[today] = None
                database.update_collection_by_cost(collection_flats, flat_page_url, cost_square_meter)
        except KeyError:
            collection_flats.update_one({"url": flat_page_url}, {"$set": {"exposed": False}})
    else:
        data = get_flat_data(flat_page_url)
        if data:
            database.insert_data_in_collection(collection_flats, data)


def update_collection_with_page(url, collection_flats):
    """
    Updates the cost of apartments on a page with a preview of apartments.
    :param url: URL of page with flat.
    :param collection_flats: Collection of flats in data base.
    :return: None
    """
    today = datetime.now()
    today = datetime(today.year, today.month - 1, today.day, today.hour, today.minute, today.second).strftime(
        "%Y:%m:%d:%H:%M:%S")
    page_html = get_html(url)
    page_soap = BeautifulSoup(page_html, "lxml")
    try:
        flats = page_soap.find("div", class_="tx-uedb").find_all("div", class_="bd-item")

        for flat in flats:
            update_collection_with_flat(flat, today, collection_flats)

    except KeyError:
        return


def get_urls_to_remove(flats):
    urls = []
    for flat in flats:
        url = flat["url"]
        html = get_html(url)
        soap = BeautifulSoup(html, "lxml")
        error_msg = soap.find("div", class_="image-404")
        if error_msg:
            urls.append(url)
    return urls


if __name__ == "__main__":
    get_flat_data("https://realt.by/grodno-region/sale/flats/object/1171874/")
