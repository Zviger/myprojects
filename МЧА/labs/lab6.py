import sympy
import pylab
import numpy as np

dx = 0.01
fileiter = open('/home/zviger/PycharmProjects/МЧА/labs/functionvalues')
xn = fileiter.__next__().rstrip().split(" ")
xn = list(map(float, xn))
yn = fileiter.__next__().rstrip().split(" ")
yn = list(map(float, yn))
x_sym = sympy.Symbol('x')
n = 5
L = sympy.numer(0)
for i in range(n):
    p = 1
    for j in range(n):
        if not i == j:
            p *= (x_sym - xn[j]) / (xn[i] - xn[j])
    L += p * yn[i]
print('Интерполяционный многочлен Лагранжа:\n', L.as_poly(), end='\n\n')
print('L4(x1+x2):\n', L.subs(x_sym, xn[0] + xn[1]), end='\n\n')


def func(x):
    return L.subs(x_sym, x)


xlist = np.arange(0, 3, dx)
ylist = [func(x) for x in xlist]
pylab.plot(xlist, ylist, label='Лагранж')
#  pylab.show()

print('Xk:\n', xn, end='\n\n')
print('Yk:\n', yn, end='\n\n')
array = yn.copy()
for j in range(4):
    for i in range(len(array) - 1):
        array[i] = array[i+1] - array[i]
    array.pop()
    print('Конечная разность {0}го порядка:'.format(j+1))
    print(array, end='\n\n')

print('Xk:\n', xn, end='\n\n')
print('Yk:\n', yn, end='\n\n')
array = yn.copy()
N = sympy.numer(yn[0])
for j in range(4):
    for i in range(len(array) - 1):
        array[i] = (array[i+1] - array[i]) / (xn[i + 1 + j] - xn[i])
    array.pop()
    a = 1
    for k in range(j + 1):
        a *= (x_sym - xn[k])
    N += array[0] * a
    print('Разделённая разность {0}го порядка:'.format(j+1))
    print(array, end='\n\n')
x_sym = sympy.Symbol('x')
print('Полином Ньютона:\n', N.as_poly(), end='\n\n')
print('N4(x1+x2):\n', N.subs(x_sym, xn[0] + xn[1]), end='\n\n')


def func(x):
    return N.subs(x_sym, x)


xlist = np.arange(0, 3, dx)
ylist = [func(x) for x in xlist]
pylab.plot(xlist, ylist, label='Ньютон')
#  pylab.show()


an = [((yn[i] - yn[i - 1]) / (xn[i] - xn[i - 1])) for i in range(1, n)]
bn = [yn[i - 1] - an[i-1] * xn[i - 1] for i in range(1, n)]
print('Кусочно-линейный сплайн')
for i in range(len(an)):
    def func(x):
        return sympy.numer(an[i] * x_sym + bn[i]).subs(x_sym, x)
    xlist = np.arange(xn[i], xn[i+1], dx)
    ylist = [func(x) for x in xlist]
    if i:
        pylab.plot(xlist, ylist, color='red')
    else:
        pylab.plot(xlist, ylist, color='red', label='кус.-лин.')
    print('{0}, {1} <= x <= {2}'.format(an[i] * x_sym + bn[i], xn[i], xn[i + 1]))
#  pylab.show()

solves = []
for k in range(1, len(xn) // 2 + 1):
    M = np.array([[xn[2*k-2] ** 2, xn[2*k-2], 1], [xn[2*k-1] ** 2, xn[2*k-1], 1], [xn[2*k] ** 2, xn[2*k], 1]])
    v = np.array([yn[2*k-2], yn[2*k-1], yn[2*k]])
    solves.append(np.linalg.solve(M, v))
print()
print('Кусочно-квадратичный сплайн:')

for i in range(len(solves)):
    def func(x):
        return sympy.numer(solves[i][0] * x_sym ** 2 + solves[i][1] * x_sym + solves[i][2]).subs(x_sym, x)
    xlist = np.arange(xn[i * 2], xn[i * 2+2], dx)
    ylist = [func(x) for x in xlist]
    if i:
        pylab.plot(xlist, ylist, color='green')
    else:
        pylab.plot(xlist, ylist, color='green', label='кус.-квад.')
    print('{0}, {1} <= x <= {2}'.format(solves[i][0] * x_sym ** 2 + solves[i][1] * x_sym + solves[i][2], xn[i], xn[i + 1]))
#  pylab.show()

hn = [xn[i] - xn[i - 1] for i in range(1, len(xn))]
ln = [(yn[i] - yn[i - 1]) / hn[i - 1] for i in range(1, len(xn))]
delta = [(-1 / 2) * hn[1] / (hn[0] + hn[1]), 0, 0, 0]
_lambda = [3/2 * (ln[1] - ln[0]) / (hn[0] + hn[1]), 0, 0, 0]
for i in range(1, len(hn) - 1):
    delta[i] = -1 * hn[i + 1] / (2 * hn[i] + 2 * hn[i + 1] + hn[i] * delta[i - 1])
    _lambda[i] = (2 * ln[i + 1] - 3 * ln[i] - hn[i + 1] * _lambda[i - 1]) / (2 * hn[i] + 2 * hn[i + 1] + hn[i] * delta[i - 1])
n = len(xn) - 1
c = [0.0] * n
for i in range(-2, -n-1, -1):
    c[i] = delta[i]*c[i+1] + _lambda[i]
b = [0.0] * n
d = [0.0] * n
b[0] = ln[0]+2*c[0]*hn[0]/3
d[0] = c[0]/(3*hn[0])
for i in range(1, n):
    b[i] = ln[i] + (2*c[i]*hn[i] + hn[i]*c[i-1])/3
    d[i] = (c[i] - c[i-1]) / (3*hn[i])
a = yn[1:]
print('\nКубический интерполяционный сплайн:')

for i in range(len(xn) - 1):
    def func(x):
        return sympy.numer(a[i] + b[i] * (x_sym - xn[i + 1]) + c[i] * (x_sym - xn[i + 1]) ** 2 + d[i] * (x_sym - xn[i + 1]) ** 3).subs(x_sym, x)
    xlist = np.arange(xn[i], xn[i + 1], dx)
    ylist = [func(x) for x in xlist]
    if i:
        pylab.plot(xlist, ylist, color='purple')
    else:
        pylab.plot(xlist, ylist, color='purple', label='кубический')
    print('{0}, {1} <= x <= {2}'.format(a[i] + b[i] * (x_sym - xn[i + 1]) + c[i] * (x_sym - xn[i + 1]) ** 2 + d[i] * (x_sym - xn[i + 1]) ** 3, xn[i], xn[i + 1]))
pylab.legend()
pylab.show()
