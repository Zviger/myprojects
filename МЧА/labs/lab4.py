import numpy as np
import math
m = None
i = 0
dim = 0
for string in open('matrix'):
    try:
        a = string.rstrip().split(' ')
        if m is not None:
            m[i] = a
        else:
            dim = len(a) - 1
            m = np.zeros((dim, dim + 1))
            m[i] = a
    except ValueError:
        exit('gg')
    i += 1
A = np.transpose(m[:, :-1]) @ m[:, : -1]
Uk = None
while True:
    max_el = 0
    i0 = 0
    j0 = 1
    i = 0
    while i < dim:
        j = i + 1
        while j < dim:
            if abs(max_el) < math.fabs(A[i][j]):
                max_el = A[i][j]
                i0 = i
                j0 = j
            j += 1
        i += 1
    if not max_el:
        break
    U = np.zeros((dim, dim))
    for i in range(dim):
        U[i, i] = 1
    a = 1/2 * math.atan(2 * max_el / (A[i0][i0] - A[j0][j0]))
    U[j0][j0] = math.cos(a)
    U[i0][i0] = math.cos(a)
    U[i0][j0] = -math.sin(a)
    U[j0][i0] = math.sin(a)
    if Uk is not None:
        Uk = Uk @ U
    else:
        Uk = U
    A = np.transpose(U) @ A @ U
    for i in range(dim):
        for j in range(dim):
            if abs(A[i][j]) < 10 ** -14:
                A[i][j] = 0
print(A, end="\n\n")
print(Uk)

