import pylab
import math
from scipy.misc import derivative
from matplotlib import mlab
import numpy as np


def func(x):
    return 2 - x - math.sin(x/4)


dx = 0.01
xlist = mlab.frange(-20.0, 20.0, dx)
Ox = [0 for x in xlist]
ylist = [func(x) for x in xlist]
pylab.plot(xlist, ylist)
pylab.plot(xlist, Ox)
pylab.axvline(0)
pylab.show()

epsilon = 10 ** -10
x = 5
x_previous = 0
x_very_previous = 0
dx = 10000
while abs(dx) > epsilon:
    x_very_previous = x_previous
    x_previous = x
    x -= func(x_previous) * (x_previous - x_very_previous) / (func(x_previous) - func(x_very_previous))
    dx = x - x_previous
print('Решение методом хорд:\n', x)
x = 0.2
if derivative(func, x, n=2) * func(x) <= 0:
    print("Этот 'икс' не подходит")
else:
    dx = 1
    while abs(dx) > epsilon:
        x_previous = x
        x -= func(x) / derivative(func, x)
        dx = x - x_previous
    print('Решение методом касательных:\n', x)


def diff_system(x):
    X = x[0]
    Y = x[1]
    Df = np.zeros((2, 2))
    Df[0, 0] = math.cos(X + Y) - 1.3
    Df[0, 1] = math.cos(X + Y)
    Df[1, 0] = 1.6 * X
    Df[1, 1] = 4 * Y
    return Df


def system(x):
    X = x[0]
    Y = x[1]
    f = np.array([math.sin(X + Y) - 1.3 * X, 0.8 * X ** 2 + 2 * Y ** 2 - 1])
    return f


x = np.array([-0.5, -0.4])
W = np.zeros((2, 2))
dx = 100000
tmp = 0
while dx > epsilon:
    x_previous = np.copy(x)
    x -= np.linalg.inv(diff_system(x)) @ system(x_previous)
    dx = max(abs(x - x_previous))
    tmp += 1
print('Решение системы методом Ньютона:\n', x)
print('Подставим x и y:\n', (system(x)))
print('Кол-во итераций:\n', tmp)


def F(x):
    X = x[0]
    Y = x[1]
    f = np.array([math.sin(X + Y)/1.3, math.sqrt((1 - 0.8 * X ** 2) / 2)])
    return f


x = np.array([-0.5, -0.4])
dx = 100000
tmp = 0
while dx > epsilon:
    x_previous = np.copy(x)
    x = F(x_previous)
    dx = max(abs(x - x_previous))
    tmp += 1
print('Решение системы методом простых итераций:\n', x)
print('Подставим x и y:\n', (system(x)))
print('Кол-во итераций:\n', tmp)


x = np.array([0.5, 0.4])
inv_W = np.linalg.inv(diff_system(x))
dx = 100000
tmp = 0
while dx > epsilon:
    x_previous = np.copy(x)
    x -= inv_W @ system(x_previous)
    dx = max(abs(x - x_previous))
    tmp += 1
print('Решение системы улучшенным методом Ньютона\n', x)
print('Подставим x и y:\n', (system(x)))
print('Кол-во итераций:\n', tmp)
