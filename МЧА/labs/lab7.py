import sympy as sp
import math
a = 1
b = 2
x = sp.Symbol('x')
#  deriv1 = 2 * x * math.log(x, math.e) + x
#  deriv2 = 2 * math.log(x, math.e) + 3
#  deriv3 = 2 / x
#  deriv4 = -2 /(x ** 2)


def f(x):
    return x ** 2 * math.log(x, math.e)


i = 1
while True:
    n = 4 * i
    dx = (b - a) / n
    M = max(map(math.fabs, [2 * math.log(a + dx * i, math.e) + 3 for i in range(n + 1)]))
    if 0.001 > M * (b - a) ** 2 * dx ** 2 / 12:
        break
    i += 1
print('Шаг интегрирования:\n', dx)

integral_h = dx * ((f(a) + f(b)) / 2 + sum([f(a + dx * i) for i in range(n)[1:]]))
integral_2h = dx * 2 * ((f(a) + f(a + n // 2 * dx)) / 2 + sum([f(a + dx * i) for i in range(n // 2)[1:]]))
print('Интеграл по формуле трапеций с шагом h:\n', integral_h)
print('Интеграл по формуле трапеций с шагом 2h:\n', integral_2h)
print('Погрешность по Рунге:\n', (integral_h - integral_2h) / (2 ** 4 - 1))

i = 1
while True:
    n = 4 * i
    dx = (b - a) / n
    M = max(map(math.fabs, [-2 / ((a + dx * i) ** 2) for i in range(n + 1)]))
    if 0.001 > M * (b - a) ** 2 * dx ** 4 / 2880:
        break
    i += 1
print('Шаг интегрирования:\n', dx)

integral_h = dx / 3 * (f(a) + f(a + n * dx) + 2 * sum([f(a + dx * i) for i in range(2, n, 2)]) + 4 * sum([f(a + dx * i) for i in range(1, n, 2)]))
integral_2h = 2 * dx / 3 * (f(a) + f(a + n // 2 * dx) + 2 * sum([f(a + dx * i) for i in range(2, n // 2 - 1, 2)]) + 4 * sum([f(a + dx * i) for i in range(1, n // 2, 2)]))
print('Интеграл по формуле Симпсона с шагом h:\n', integral_h)
print('Интеграл по формуле Симпсона с шагом 2h:\n', integral_2h)
print('Погрешность по Рунге:\n', (integral_h - integral_2h) / (2 ** 4 - 1))

print('По формуле Ньютона-Лейбница интеграл равен:\n', 1.0706147037)
