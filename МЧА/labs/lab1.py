import numpy as np
m = None
i = 0
dim = 0
for string in open('matrix'):
    try:
        a = string.rstrip().split(' ')
        if m is not None:
            m[i] = a
        else:
            dim = len(a) - 1
            m = np.zeros((dim, dim + 1))
            m[i] = a
    except ValueError:
        exit('gg')
    i += 1
mCopy = m.copy()
print('Матрица:\n{0}'.format(m))
if abs(np.linalg.det((m[:, :-1]))) < abs(1e-10):
    print('Эта система имеет бесконечное множество решений :)')
    exit(0)
invM = np.linalg.inv(m[:, :-1])
print('Обратная матрица:\n{0}'.format(invM))
for i in range(dim):
    if m[i][i] == 0:
        for k in range(dim)[i+1:]:
            if m[k][i] != 0:
                m[k][i], m[i][i] = m[i][i], m[k][i]
                break
    for j in range(dim)[i+1:]:
        m[j] += m[i] * -(m[j][i]/m[i][i])
print('Матрица, приведённая к треугольному виду:\n{0}'.format(m))
sol = [0] * dim
for i in range(dim)[::-1]:
    a = 0
    if sol:
        for j in range(dim)[i+1:]:
            a += m[i, j]*sol[j]
    sol[i] = 1/m[i, i]*(m[i,  dim] - a)
print('Множество решений системы:\n' + str(sol))
print('Абсолютная погрешность решения системы равна:\n{0}'.format(0.001 * np.max([np.sum([abs(j) for j in i]) for i in invM])))
print('Относительная погрешность решения системы меньше или равна:')
print(np.max([sum(map(abs, i)) for i in invM]) * max([sum(map(abs, i)) for i in mCopy[:, :-1]]) * 0.001 / max(map(abs, np.transpose(mCopy)[dim])))
