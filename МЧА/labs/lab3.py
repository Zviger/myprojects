import numpy as np
import math
m = None
i = 0
dim = 0
for string in open('matrix'):
    try:
        a = string.rstrip().split(' ')
        if m is not None:
            m[i] = a
        else:
            dim = len(a) - 1
            m = np.zeros((dim, dim + 1))
            m[i] = a
    except ValueError:
        exit('gg')
    i += 1
A = np.transpose(m[:, :-1]) @ m[:, : -1]
print('Симметричная матрица:\n', A)
b = np.transpose(m[:, :-1]) @ m[:, -1:]
print('и её свободные члены:\n', b)
U = np.zeros((dim, dim))
for i in range(dim):
    try:
        g = sum([row[i] ** 2 for row in U[: i]])
        U[i, i] = math.sqrt(A[i, i] - sum([row[i] ** 2 for row in U[: i]]))
    except ValueError:
        print('Не очень красивая матрица для данного метода ))00))0')
        exit()
    for j in range(dim):
        if i != j:
            U[i, j] = (A[i, j] - sum([row[i] * row[j] for row in U][: i])) / U[i, i]
for i in range(dim):
    for j in range(dim)[: i]:
        U[i][j] = 0
print('Матрица U:\n{0}'.format(U))
y = np.linalg.inv(np.transpose(U)) @ b
print('Решение системы методом квадратного корня:\n{0}'.format(np.linalg.inv(U) @ y))
detA = 1
for i in range(dim):
    detA *= U[i][i] ** 2
print('Определитель матрицы:\n{0}'.format(detA))
invA = np.zeros((dim, dim))
for i in range(dim):
    e_i = np.array([[0]] * dim)
    e_i[i] = 1
    invY = np.linalg.inv(np.transpose(U)) @ e_i
    invX = np.linalg.inv(U) @ invY
    for j in range(dim):
        invA[j, i] = invX[j]
print('Обратная матрица:\n{0}'.format(invA))
