import numpy as np
import math
m = None
i = 0
dim = 0
for string in open('matrix'):
    try:
        a = string.rstrip().split(' ')
        if m is not None:
            m[i] = a
        else:
            dim = len(a) - 1
            m = np.zeros((dim, dim + 1))
            m[i] = a
    except ValueError:
        exit('gg')
    i += 1
mCopy = m.copy()
print('Матрица:\n{0}'.format(m))
invM = np.linalg.inv(m[:, :-1])
if abs(np.linalg.det((m[:, :-1]))) < abs(1e-10):
    print('Эта система имеет бесконечное множество решений :)')
    exit(0)
for i in range(dim):
    if m[i][i] == 0:
        for k in range(dim)[i+1:]:
            if m[k][i] != 0:
                m[k][i], m[i][i] = m[i][i], m[k][i]
                break
    for j in range(dim)[i+1:]:
        mCopy[j] += mCopy[i] * -(mCopy[j][i]/mCopy[i][i])
for i in range(dim - 1, -1, -1):
    for j in range(i - 1, -1, -1):
        mCopy[j] += mCopy[i] * -(mCopy[j][i] / mCopy[i][i])
for i in range(dim):
    m[i] /= -m[i][i]
    m[i][dim] *= -1
    m[i][i] = 0
    if mCopy[i][i] <= 0:
        print('МПИ не сходится')
        exit(0)
print('Представим систему в виде x = B*x + c...\nМатрица В:\n{0}'.format(m[:, :-1]))
print('Вектор с:\n{0}'.format(m[:, -1:]))
currentV = np.array([[0]] * dim)
acc = 0.0001
pastV = None
normB = max([sum([abs(j) for j in i]) for i in m[:, :-1]])
n = 0
while True:
    pastV = currentV.copy()
    currentV = m[:, :-1] @ pastV + m[:, -1:]
    n += 1
    if pastV is not None:
        if normB / (1 - normB) * max(abs(currentV - pastV)) < acc:
            break
print('Кол-во итераций:\n{0}'.format(n+1))
print('Вектор решения системы методом простых итераций с точностью до {1}:\n{0}'.format(currentV, acc))
H = m[:, :-1].copy()
F = m[:, :-1].copy()
for i in range(dim)[:]:
    H[i] *= -1
    H[i][i] = 1
    for j in range(dim)[i+1:]:
        H[i][j] = 0
for i in range(dim - 1, -1, -1)[:-1]:
    for j in range(dim)[: i]:
        F[i][j] = 0
H = np.linalg.inv(H)
currentV = np.array([[0]] * dim)
pastV = None
n = 0
while True:
    pastV = currentV.copy()

    currentV = H @ F @ pastV + H @ m[:, -1:]
    n += 1
    if pastV is not None:
        if normB / (1 - normB) * max(abs(currentV - pastV)) < acc:
            break
print('Кол-во итераций:\n{0}'.format(n+1))
print('Вектор решения системы методом Зейделя итераций с точностью до {1}:\n{0}'.format(currentV, acc))
k = 0
a = np.max([abs(i) for i in (m[:, -1:] - np.array([[0]] * dim))])

print(math.log(acc * (normB ** k) * (1 - normB) / (np.max([abs(i) for i in (m[:, -1:] - np.array([[0]] * dim))]))) / math.log(normB))
