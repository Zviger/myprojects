import pickle

class Individual:

    def __init__(self, name, adds, phone_num):
        self.__adds = adds
        self.__phone_num = phone_num
        self.__name = name


class User(Individual):

    def __init__(self, login, password, name, adds, phone_num):
        super().__init__(name, adds, phone_num)
        self.__login = login
        self.__password = password

    def change_login(self, login):
        self.__login = login

    def change_password(self, password):
        self.__password = password

    def login_in(self, login, password):
        pass

    def set_id(self):
        pass


class Customer(User):

    def __init__(self, login, password, name, adds, phone_num):
        super().__init__(login, password, name, adds, phone_num)
        self.__purchase_history = []


class Seller(User):

    def __init__(self, login, password, name, adds, phone_num):
        super().__init__(login, password, name, adds, phone_num)


class BicyclePart:

    def __init__(self, name, vendor_code, amount=0):
        self.__name = name
        self.__vendor_code = vendor_code
        self.__amount = amount

    def get_code(self):
        return self.vendor_code

    def get_amount(self):
        return self.amount

    def increase_amount(self, value=0):
        self.amount += value


class Producer(Individual):

    def __init__(self, company_name, adds, phone_num):
        super().__init__(company_name, adds, phone_num)
        self.catalog = []

    def add_parts(self, parts):
        for part in parts:
            for company_part in self.catalog:
                if part.get_code() == company_part.get_code():
                    company_part.increase_amount(part.get_amount)
            else:
                self.catalog.append(part)


class Application:

    def __init__(self):
        self.__sellers = []
        self.__customers = []
        try:
            with open('sellers', 'rb') as f:
                self.__sellers = pickle.load(f)
        except EOFError:
            self.__sellers = []
        try:
            with open('customers', 'rb') as f:
                self.__customers = pickle.load(f)
        except EOFError:
            self.__customers = []

        self.__current_seller = None
        self.__current_customer = None

    def set_current_user(self, current_user):
        if current_user is Seller:
            self.__current_seller = current_user
            self.__current_customer = None
        elif current_user is Customer:
            self.__current_seller = None
            self.__current_customer = current_user
        else:
            self.__current_seller = None
            self.__current_customer = None

    def registration(self, login, password, name, adds, phone_num, whoisu="customer"):

        if login in [seller.__login for seller in self.__sellers]:
            return False
        elif login in [customer.__login for customer in self.__customers]:
            return False
        elif whoisu == "customer":
            self.__customers.append(Customer(login, password, name, adds, phone_num))
            return True
        elif whoisu == "seller":
            self.__sellers.append(Seller(login, password, name, adds, phone_num))
            return True
        else:
            return False

    def log_in(self, login, password):
        for seller in self.__sellers:
            if seller.__login == login:
                if seller.__password == password:
                    self.set_current_user(seller)
                    return ""
                else:
                    return "invalid password"
        for customer in self.__customers:
            if customer.__login == login:
                if customer.__password == password:
                    self.set_current_user(customer)
                    return ""
                else:
                    return "invalid password"
        return "invalid login"
