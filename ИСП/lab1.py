import random
import numpy as np


class Game:
    def __init__(self, size):
        self.size = size
        self.board = np.zeros((self.size, self.size))
        for i in range(self.size):
            for j in range(self.size):
                self.board[i][j] = i * self.size + j

    def print_board(self):
        print(self.board)

    def player_move(self):
        numbers = []
        try:
            numbers = list(map(int, input().rstrip().split()))
        except ValueError:
            print('Да, да, сейчас бы не числа вводить... Бан хочешь?')
        return numbers

    def computer_mode(self):
        while True:
            f_i = random.randint(1, self.size - 2)
            f_j = random.randint(1, self.size - 2)
            if self.board[f_i][f_j] != -1:
                numbers = [f_i * self.size + f_j]
                s_i = f_i + random.choice([-1, 1])
                s_j = f_j + random.choice([-1, 1])
                if self.board[s_i][s_j] != -1:
                    numbers.append(s_i * self.size + s_j)
                    choice = random.choice([1, 2])
                    if choice == 1:
                        if self.board[s_i][f_j] != -1:
                            numbers.append(s_i * self.size + f_j)
                            break
                    else:
                        if self.board[f_i][s_j] != -1:
                            numbers.append(f_i * self.size + s_j)
                            break
        return numbers

    def start(self):
        print('Данная игра рассчитана на 2х человек.\n'
              'В чём суть? Расставляем тримино на поле 8х8, пока не кончится место.'
              ' Кто последним ставил, тот и выиграл.\nЧто такое тримино? Штука такая в форме ГЭ.\n'
              'Как играть? Пишешь через пробел три числа, которые описывают путь рисовки тримино.\nУдачи :)')
        player = 1
        while True:
            correct = True
            self.print_board()

            print('Игрок №{0}:'.format(player))
            if player == 1:
                selected_numbers = self.player_move()
                if not selected_numbers:
                    continue
            else:
                selected_numbers = self.computer_mode()

            if len(selected_numbers) != 3:
                print("Тебе ясно дали понять, что нужно вводить 3 числа")
                continue
            for num in selected_numbers:
                if not 0 <= num < self.size * self.size:
                    correct = False

            indexes = [[num // self.size, num - num // self.size * self.size] for num in selected_numbers]

            for i in range(3):
                if self.board[indexes[i][0], indexes[i][1]] == -1:
                    correct = False

            if not correct:
                print("Тебе просто нужно выбрать числа, куда ты хочешь поставить тримино!")
                continue

            current_indexes = [-1, -1]
            if (indexes[0][0] - 1 == indexes[1][0] and indexes[0][1] - 1 == indexes[1][1]) or (
                    indexes[0][0] - 1 == indexes[2][0] and indexes[0][1] - 1 == indexes[2][1]):
                current_indexes[0] = indexes[0][0] - 1
                current_indexes[1] = indexes[0][1] - 1

            if (indexes[0][0] - 1 == indexes[1][0] and indexes[0][1] + 1 == indexes[1][1]) or (
                    indexes[0][0] - 1 == indexes[2][0] and indexes[0][1] + 1 == indexes[2][1]):
                current_indexes[0] = indexes[0][0] - 1
                current_indexes[1] = indexes[0][1] + 1

            if (indexes[0][0] + 1 == indexes[1][0] and indexes[0][1] - 1 == indexes[1][1]) or (
                    indexes[0][0] + 1 == indexes[2][0] and indexes[0][1] - 1 == indexes[2][1]):
                current_indexes[0] = indexes[0][0] + 1
                current_indexes[1] = indexes[0][1] - 1

            if (indexes[0][0] + 1 == indexes[1][0] and indexes[0][1] + 1 == indexes[1][1]) or (
                    indexes[0][0] + 1 == indexes[2][0] and indexes[0][1] + 1 == indexes[2][1]):
                current_indexes[0] = indexes[0][0] + 1
                current_indexes[1] = indexes[0][1] + 1

            if current_indexes[0] == -1:
                print("В тетрис играл? Видимо, нет. Букву ГЭ набери, а не ерунду всякую")
                continue

            if (indexes[0][0] == indexes[1][0] and current_indexes[1] == indexes[1][1]) or (
                    indexes[0][0] == indexes[2][0] and current_indexes[1] == indexes[2][1]):
                pass
            else:
                if (indexes[0][1] == indexes[1][1] and current_indexes[0] == indexes[1][0]) or (
                            indexes[0][1] == indexes[2][1] and current_indexes[0] == indexes[2][0]):
                    pass
                else:
                    print("В тетрис играл? Видимо, нет. Букву ГЭ набери, а не ерунду всякую")
                    continue

            for index in indexes:
                self.board[index[0], index[1]] = -1
            self.cls()

            end_game = True
            for i in range(self.size)[1:-1]:
                for j in range(self.size)[:-1]:
                    if self.board[i][j] != -1:
                        if self.board[i][j + 1] != -1:
                            if self.board[i - 1][j] == -1 and self.board[i + 1][j] == -1 and self.board[i - 1][j + 1] == -1 and self.board[i + 1][j + 1] == -1:
                                continue
                            else:
                                end_game = False
                                break
                        else:
                            continue
                    else:
                        continue

            if end_game:
                print("Выиграл игрок №{0}".format(player))
                exit()

            if player == 1:
                player = 2
            else:
                player = 1

    def cls(self):
        print('\n' * 100)


if __name__ == "__main__":
    game = Game(8)
    game.start()
