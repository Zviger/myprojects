def countLines(file):
    print('Lines: ', len(file.readlines()))


def countChars(file):
    chars = 0
    for line in file.readlines():
        chars += len(line)
    print('Chars: ', chars)


def test(name):
    file = open(name)
    countLines(file)
    file.seek(0)
    countChars(file)
